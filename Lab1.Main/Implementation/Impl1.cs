﻿using Lab1.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    public class Impl1:IKobieta
    {
        public Impl1()
        { }

        public void ZjedzJablko()
        {
            string jablko = "jablko";
            Console.WriteLine(jablko);
        }

        public void PrzedstawSie()
        {
            int j = 5;
            for (int i = 0; i < j; i++)
            {
                Console.WriteLine("Zjedz to jablko");
            }
        }


        public string Zwracaj()
        {
            return "Zjadlam jablko";
        }
    }
}
