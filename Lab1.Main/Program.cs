﻿using Lab1.Contract;
using Lab1.Implementation;
using System;
using System.Collections.Generic;

namespace Lab1.Main
{
    public class Program
    {
        static void Main(string[] args)
        {

            

            var program = new Program();

            var lista = program.Dodaj();


            program.RobCos(lista);


            var _one = new Impl3();
            IKobieta _two = new Impl3();
            IKosmita _three = new Impl3();

            Console.WriteLine(_one.Zwracaj());
            Console.WriteLine(_two.Zwracaj());
            Console.WriteLine(_three.Zwracaj());
        }

        public IList<ICzlowiek> Dodaj()
        {
            var lista = new List<ICzlowiek>();

            var pierwszy = new Impl1();
            var drugi = new Impl2();

            var trzeci = new Impl2();

            lista.Add(pierwszy);
            lista.Add(drugi);
            lista.Add(trzeci);

            return lista;
        }

        public void RobCos(IList<ICzlowiek> collection)
        {
            foreach (var item in collection)
            {
                item.PrzedstawSie();
            }
        }
    }
}
